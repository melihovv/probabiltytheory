#ifndef TEST_H
#define TEST_H

#include <QObject>
#include <tuple>
#include "testsrunner.h"

typedef std::tuple<double, double, double> Coeffs;
Q_DECLARE_METATYPE(Coeffs);

class TestSelectiveRegressionEquationFinder : public QObject
{
    Q_OBJECT

private slots:
    void testCalc_data();
    void testCalc();
};

DECLARE_TEST(TestSelectiveRegressionEquationFinder);

#endif // TEST_H
