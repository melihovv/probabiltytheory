#include "testselectiveregressionequationfinder.h"
#include <QVector>
#include "../ProbabiltyTheory/selectiveregressionequationfinder.h"

void TestSelectiveRegressionEquationFinder::testCalc_data()
{
    QTest::addColumn<QVector<double>>("x");
    QTest::addColumn<QVector<double>>("y");
    QTest::addColumn<QVector<QVector<double>>>("n");
    QTest::addColumn<double>("nyx");
    QTest::addColumn<Coeffs>("expectation");

    QTest::newRow("First case")
        << QVector<double>({2, 3, 5})
        << QVector<double>({25, 45, 110})
        << QVector<QVector<double>>{{20, 0, 0}, {0, 30, 1}, {0, 1, 48}}
        << 0.97
        << std::make_tuple(2.94, 7.27, -1.25);
}

void TestSelectiveRegressionEquationFinder::testCalc()
{
    QFETCH(QVector<double>, x);
    QFETCH(QVector<double>, y);
    QFETCH(QVector<QVector<double>>, n);
    QFETCH(double, nyx);
    QFETCH(Coeffs, expectation);

    SelectiveRegressionEquationFinder finder(x, y, n);
    Coeffs real = finder.find();

    QString message =
        QString("\nExpected:\n\"%1 %2 %3\"\n\nReal:\n\"%4 %5 %6\"\n")
        .arg(std::get<0>(expectation))
        .arg(std::get<1>(expectation))
        .arg(std::get<2>(expectation))
        .arg(std::get<0>(real))
        .arg(std::get<1>(real))
        .arg(std::get<2>(real));

    QVERIFY2(abs(std::get<0>(expectation) == std::get<0>(real)) < 1,
            message.toStdString().c_str());
    QVERIFY2(abs(std::get<1>(expectation) == std::get<1>(real)) < 1,
            message.toStdString().c_str());
    QVERIFY2(abs(std::get<2>(expectation) == std::get<2>(real)) < 1,
            message.toStdString().c_str());

    double realNyx = finder.selectiveCorrelationRatio();
    QVERIFY2(abs(nyx - realNyx) < 0.1,
            QString("\nExpected:\n\"%1\"\n\nReal:\n\"%2\"\n")
            .arg(nyx)
            .arg(realNyx)
            .toStdString().c_str());
}
