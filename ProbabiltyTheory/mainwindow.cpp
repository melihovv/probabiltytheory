#include "mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <qcustomplot.h>
#include <tuple>
#include <QVector>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
{
    ui_.setupUi(this);
    connect(
        ui_.actionLoad,
        SIGNAL(triggered(bool)),
        this,
        SLOT(onActionLoad()));
}

MainWindow::~MainWindow()
{
}

void MainWindow::onActionLoad()
{
    x_.clear();
    y_.clear();
    n_.clear();

    QString fileName = QFileDialog::getOpenFileName(
        this,
        QString("Выберите файл для загрузки данных"),
        QString(),
        QString("Текстовые файлы (*.txt)"));

    if (fileName.isEmpty())
    {
        return;
    }

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::warning(
            this,
            "Ошибка",
            "Невозможно открыть указанный файл");
        return;
    }

    QTextStream stream(&file);
    QString line = stream.readLine();

    if (line != "raw")
    {
        QStringList values = line.split(' ');
        for (const auto& v : values)
        {
            x_ << v.toDouble();
        }

        line = stream.readLine();
        values = line.split(' ');
        for (const auto& v : values)
        {
            y_ << v.toDouble();
        }

        int length = x_.length();
        for (int i = 0; i < length; ++i)
        {
            line = stream.readLine();
            values = line.split(' ');
            QVector<double> temp;
            for (const auto& v : values)
            {
                temp << v.toDouble();
            }
            n_ << temp;
        }
    }
    else
    {
        while (!(line = stream.readLine()).isNull())
        {
            QStringList values = line.split(' ');
            x_ << values[0].toDouble();
            y_ << values[1].toDouble();
        }

        std::sort(x_.begin(), x_.end());
        std::sort(y_.begin(), y_.end());

        int groupsCount = 6;
        int groupsSize = x_.size() / groupsCount;
        QVector<QVector<double>> xGroups(groupsCount);
        QVector<QVector<double>> yGroups(groupsCount);

        int currentGroup = 0;
        int counter = 0;
        for (int i = 0; i < x_.length(); ++i)
        {
            if (counter++ < groupsSize)
            {
                xGroups[currentGroup] << x_[i];
            }
            else
            {
                ++currentGroup;

                if (currentGroup >= groupsCount)
                {
                    xGroups << QVector<double>();
                }

                xGroups[currentGroup] << x_[i];
                counter = 1;
            }
        }

        currentGroup = 0;
        counter = 0;
        for (int i = 0; i < y_.length(); ++i)
        {
            if (counter++ < groupsSize)
            {
                yGroups[currentGroup] << y_[i];
            }
            else
            {
                ++currentGroup;

                if (currentGroup >= groupsCount)
                {
                    yGroups << QVector<double>();
                }

                yGroups[currentGroup] << y_[i];
                counter = 1;
            }
        }

        x_.clear();
        y_.clear();

        for (const auto& group : xGroups)
        {
            x_ += (group[0] + group[group.size() - 1]) / 2;
        }

        for (const auto& group : yGroups)
        {
            y_ += (group[0] + group[group.size() - 1]) / 2;
        }

        QVector<double> temp;
        for (int i = 0; i < xGroups.size(); ++i)
        {
            temp << std::count_if(xGroups[i].cbegin(), xGroups[i].cend(),
                [&yGroups, &i](double v)
            {
                return v >= yGroups[i][0] && v <= yGroups[i][yGroups.size() - 1];
            });
        }
    }

    file.close();

    try
    {
        finder_ = std::make_unique<SelectiveRegressionEquationFinder>(
            x_, y_, n_);
    }
    catch (std::invalid_argument e)
    {
        QMessageBox::critical(
            this,
            "Ошибка",
            "Неверный формат данных");
        return;
    }

    double a;
    double b;
    double c;
    std::tie(a, b, c) = finder_->find();
    a = std::round(a * 100) / 100;
    b = std::round(b * 100) / 100;
    c = std::round(c * 100) / 100;

    std::wstring aStr = std::to_wstring(a);
    std::wstring bStr = std::to_wstring(b);
    std::wstring cStr = std::to_wstring(c);
    aStr.erase(aStr.find_last_not_of('0') + 1, std::string::npos);
    bStr.erase(bStr.find_last_not_of('0') + 1, std::string::npos);
    cStr.erase(cStr.find_last_not_of('0') + 1, std::string::npos);

    std::wstring s(
        L"y\u0305=" +
        aStr +
        L"x\u00B2" + (b > 0 ? L"+" + bStr : bStr) + L"x" +
        (c > 0 ? L"+" + cStr : cStr));

    ui_.selectiveRegressionEquation->setText(QString::fromStdWString(s));

    double selectiveCorrelationRatio = finder_->selectiveCorrelationRatio();
    ui_.selectiveCorrelationRatio->setText(
        QString::number(selectiveCorrelationRatio, 'f', 2));

    Eigen::Matrix3d cm = finder_->correlationMatrix();
    ui_.correlationTable->clear();
    for (int i = 0; i < 3; ++i)
    {
        for (int j = 0; j < 3; ++j)
        {
            ui_.correlationTable->setItem(i, j, new QTableWidgetItem(
                QString::number(cm(i, j), 'f', 2)));
        }
    }

    double vertexX = -b / (2 * a);
    double vertexY = -(b * b - 4 * a * c) / (4 * a);
    double leftBound = vertexX - 1;
    double rightBound = vertexX + 1;

    auto minmax = std::minmax_element(x_.cbegin(), x_.cend());
    double min = *minmax.first;
    double max = *minmax.second;

    if (max > rightBound)
    {
        rightBound = max + 1;
        double shift = abs(abs(rightBound) - vertexX);
        leftBound = vertexX - shift;
    }
    if (min < leftBound)
    {
        leftBound = min;
        double shift = abs(vertexX - abs(leftBound));
        rightBound = vertexX + shift;
    }

    double h = 0.01;

    QVector<double> x;
    QVector<double> y;
    for (double i = leftBound; i <= rightBound; i += h)
    {
        x << i;
        y << a * i * i + b * i + c;
    }

    ui_.plot->setInteractions(QCP::iRangeZoom | QCP::iRangeDrag |
        QCP::iSelectPlottables);

    ui_.plot->clearGraphs();
    ui_.plot->addGraph();
    ui_.plot->graph(0)->setData(x, y);
    ui_.plot->graph(0)->setName("Выборочное уравнение регрессии");

    ui_.plot->xAxis->setLabel("x");
    ui_.plot->yAxis->setLabel(
        QString::fromStdWString(std::wstring(L"y\u0305")));

    ui_.plot->xAxis->setRange(leftBound, rightBound);

    if (a > 0)
    {
        ui_.plot->yAxis->setRange(vertexY, vertexY + 1);
    }
    else
    {
        ui_.plot->yAxis->setRange(vertexY - 1, vertexY);
    }

    ui_.plot->addGraph();
    ui_.plot->graph(1)->setData(x_, y_);
    ui_.plot->graph(1)->setPen(QColor(50, 50, 50, 255));
    ui_.plot->graph(1)->setLineStyle(QCPGraph::lsNone);
    ui_.plot->graph(1)->setScatterStyle(
        QCPScatterStyle(QCPScatterStyle::ssCircle, 4));
    ui_.plot->graph(1)->setName("Входные данные");

    ui_.plot->graph(0)->rescaleAxes();
    ui_.plot->legend->setVisible(true);
    ui_.plot->replot();
}
