#include "selectiveregressionequationfinder.h"
#include <numeric>

SelectiveRegressionEquationFinder::SelectiveRegressionEquationFinder(
    const QVector<double> x,
    const QVector<double> y,
    const QVector<QVector<double>> n)
{
    if (x.size() < 1 ||
        y.size() < 1 ||
        n.size() != y.size() ||
        n[0].size() != x.size())
    {
        throw std::invalid_argument("Invalid arguments");
    }

    x_ = x;
    y_ = y;
    n_ = n;
    nx_ = QVector<double>(x_.size(), 0);
    yx_ = QVector<double>(x_.size(), 0);
}

std::tuple<double, double, double> SelectiveRegressionEquationFinder::find()
{
    // Calc nx, ny.
    int colsNum = x_.size();
    int rowsNum = y_.size();
    for (int i = 0; i < rowsNum; ++i)
    {
        double rowSum = 0;

        for (int j = 0; j < colsNum; ++j)
        {
            rowSum += n_[i][j];
            nx_[j] += n_[i][j];
        }

        ny_ << rowSum;
    }

    // Calc conditional average characteristic.
    // (sum of n_[i][j] * y_[j]) / nx[i].
    for (int i = 0; i < colsNum; ++i)
    {
        for (int j = 0; j < rowsNum; ++j)
        {
            yx_[i] += n_[i][j] * y_[j];
        }
        yx_[i] /= nx_[i];
    }


    // Calc nx * x.
    // Calc nx * x^2.
    // Calc nx * x^3.
    // Calc nx * x^4.
    QVector<double> nxx(colsNum, 0);
    QVector<double> nxx2(colsNum, 0);
    QVector<double> nxx3(colsNum, 0);
    QVector<double> nxx4(colsNum, 0);
    for (int i = 0; i < colsNum; ++i)
    {
        nxx[i] = x_[i] * nx_[i];
        nxx2[i] = nxx[i] * x_[i];
        nxx3[i] = nxx2[i] * x_[i];
        nxx4[i] = nxx3[i] * x_[i];
    }

    // Calc nx * yx.
    // Calc nx * yx * x.
    // Calc nx * yx * x^2.
    QVector<double> nxyx(colsNum, 0);
    QVector<double> nxyxx(colsNum, 0);
    QVector<double> nxyxx2(colsNum, 0);
    for (int i = 0; i < colsNum; ++i)
    {
        nxyx[i] = nx_[i] * yx_[i];
        nxyxx[i] = nxyx[i] * x_[i];
        nxyxx2[i] = nxyxx[i] * x_[i];
    }

    // Calc matrix coeffs.
    double a11 = std::accumulate(nxx4.cbegin(), nxx4.cend(), 0);
    double a12 = std::accumulate(nxx3.cbegin(), nxx3.cend(), 0);
    double a13 = std::accumulate(nxx2.cbegin(), nxx2.cend(), 0);
    double a21 = a12;
    double a22 = a13;
    double a23 = std::accumulate(nxx.cbegin(), nxx.cend(), 0);
    double a31 = a13;
    double a32 = a23;
    double a33 = std::accumulate(nx_.cbegin(), nx_.cend(), 0);
    double b1 = std::accumulate(nxyxx2.cbegin(), nxyxx2.cend(), 0);
    double b2 = std::accumulate(nxyxx.cbegin(), nxyxx.cend(), 0);
    double b3 = std::accumulate(nxyx.cbegin(), nxyx.cend(), 0);

    Eigen::Matrix3d m;
    m << a11, a12, a13, a21, a22, a23, a31, a32, a33;

    _correlationMatrix = (m.transpose() * m).inverse();

    Eigen::Vector3d b(b1, b2, b3);
    Eigen::Vector3d x = m.colPivHouseholderQr().solve(b);

    return std::make_tuple(x[0], x[1], x[2]);
}

double SelectiveRegressionEquationFinder::selectiveCorrelationRatio()
{
    double result = 0;

    // Find common average.
    // (sum of ny * y) / n.
    int size = y_.length();
    double commonAverage = 0;
    for (int i = 0; i < size; ++i)
    {
        commonAverage += ny_[i] * y_[i];
    }
    double n = std::accumulate(ny_.cbegin(), ny_.cend(), 0);
    commonAverage /= n;

    // Find common average square deviation.
    // sqrt((sum of ny * (y - commonAverage)^2) / n).
    double casd = 0;
    for (int i = 0; i < size; ++i)
    {
        casd += ny_[i] * pow(y_[i] - commonAverage, 2);
    }
    casd /= n;
    casd = sqrt(casd);

    // Find intergroup average square deviation.
    // sqrt((sum of nx * (yx - commonAverage)^2) / n).
    double iasd = 0;
    for (int i = 0; i < size; ++i)
    {
        iasd += nx_[i] * pow(yx_[i] - commonAverage, 2);
    }
    iasd /= n;
    iasd = sqrt(iasd);

    result = iasd / casd;
    return result;
}

Eigen::Matrix3d SelectiveRegressionEquationFinder::correlationMatrix()
{
    return _correlationMatrix;
}
