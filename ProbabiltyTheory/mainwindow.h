#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include "ui_mainwindow.h"
#include <memory>
#include "selectiveregressionequationfinder.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = 0);
    ~MainWindow();

private slots:
    void onActionLoad();

private:
    Ui::MainWindowClass ui_;
    QVector<double> x_;
    QVector<double> y_;
    QVector<QVector<double>> n_;
    std::unique_ptr<SelectiveRegressionEquationFinder> finder_;
};

#endif // MAINWINDOW_H
