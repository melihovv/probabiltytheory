#ifndef SELECTIVEREGRESSIONEQUATIONFINDER_H
#define SELECTIVEREGRESSIONEQUATIONFINDER_H

#include <QVector>
#include <tuple>
#include <Eigen/Dense>

class SelectiveRegressionEquationFinder
{
public:
    SelectiveRegressionEquationFinder(
        const QVector<double> x,
        const QVector<double> y,
        const QVector<QVector<double>> n) throw (std::invalid_argument);

    std::tuple<double, double, double> find();

    double selectiveCorrelationRatio();

    Eigen::Matrix3d correlationMatrix();

private:
    QVector<double> x_;
    QVector<double> y_;
    QVector<QVector<double>> n_;
    QVector<double> ny_;
    QVector<double> nx_;
    QVector<double> yx_;
    Eigen::Matrix3d _correlationMatrix;
};

#endif // SELECTIVEREGRESSIONEQUATIONFINDER_H
